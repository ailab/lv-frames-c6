#!/usr/bin/env bash

IMG=registry.gitlab.com/ailab/lv-frames-c6

if [ $# -eq 0 ]; then
    TAG=latest
    echo "Build local ${IMG}:${TAG}"
    docker build -t $IMG:$TAG .
fi

# ./make.sh push 0.1
if [ "$1" == "push" ]; then
    TAG=${2:-latest}
    echo "Push ${IMG}:${TAG}"
    docker tag $IMG:latest $IMG:$TAG \
        && docker push $IMG:$TAG
fi
