#!/usr/bin/env python3

import sys, os, zipfile, json, logging
from collections import defaultdict


basedir = os.path.dirname(__file__)

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class Dict(dict):
    def __init__(self, *args, **kwargs):
        super(Dict, self).__init__(*args, **kwargs)
        self.__dict__ = self
    def __getattribute__(self, key):
        try:
            return super(Dict, self).__getattribute__(key)
        except:
            return
    def __delattr__(self, name):
        if name in self:
            del self[name]


def convert_conll_features(features):
    if not features:
        return
    if features == '_':
        return
    output = {}
    for k,v in (kv.split('=') for kv in filter(None, features.split('|'))):
        output[k] = v
    return output


def compute_paths(tokens):
    # expect: tokens = [token], token.path = defaultdict(tuple) {output}, token.n, token.parent = n, token.children = [n]
    # collect all parent-child connections with direction
    links = tuple([] for i in range(len(tokens)))
    for token in tokens:
        if token.parent:
            links[token.n].append((token.parent, True))
        if token.children:
            for child in token.children:
                links[token.n].append((child, False))
        token.paths[token.n] = tuple()  # for consistency
    # collect all paths
    for token in tokens:
        path = [(token.n,)]
        depth = 1
        it = iter(links[token.n])
        pathiters = [iter([]), it]
        # cik ir maksimālais pāreju skaits ? uz katru mezglu pa divām: turp un atpakaļ
        for i in range(len(tokens) * 4):  # times 2 should be enough, but add some reserve
            try:
                node = next(it)
                if node[0] == path[depth-2][0]:
                    continue
                if len(path) == depth:
                    path.append(node)
                else:
                    path[depth] = node
                token.paths[node[0]] = tuple(path[1:])  # add computed path
                if len(links[node[0]]) > 1:
                    it = iter(links[node[0]])
                    pathiters.append(it)
                    depth += 1
            except StopIteration:   # next() reached end of iterator
                depth -= 1
                if depth == 0:
                    break
                path.pop()
                pathiters.pop()
                it = pathiters[depth]



def load_framenet(source):
    framenet = {}
    if type(source) is str and os.path.isfile(source):
        with open(source, 'r') as f:
            framenet = json.load(f)
    elif hasattr(source, 'read'):
        framenet = json.load(source)
    if framenet:
        framenet = {target:set(elements) for target,elements in framenet.items()}
    return framenet


def load_zipped_model(path):
    target_categories = {}
    element_categories = {}
    framenet = {}
    with zipfile.ZipFile(path) as zf:
        for p in zf.namelist():
            if not p.endswith('.json'):
                continue
            i = zf.getinfo(p)
            if i.is_dir():
                continue
            if os.path.basename(p) == 'framenet.json':
                with zf.open(p) as f:
                    framenet = load_framenet(f)
                continue
            parent = os.path.basename(os.path.dirname(p))
            if parent == 'targets':
                categories = target_categories
            elif parent == 'elements':
                categories = element_categories
            else:
                continue
            n = os.path.splitext(os.path.basename(p))[0]
            with zf.open(p) as f:
                categories[n] = json.load(f)
    for category, rules in target_categories.items():
        for rule in rules:
            if rule['class'] != category:
                raise ValueError('category does not match with rule class: %s vs %s' % (category, rule['class']))
    for category, rules in element_categories.items():
        for rule in rules:
            if rule['class'] != category:
                raise ValueError('category does not match with rule class: %s vs %s' % (category, rule['class']))
    return framenet, target_categories, element_categories


def load_categories(path):
    categories = {}
    for fn in os.listdir(path):
        if not fn.endswith('.json'):
            continue
        n = os.path.splitext(os.path.basename(fn))[0]
        p = os.path.join(path, fn)
        with open(p, 'r') as f:
            categories[n] = json.load(f)
    # verification
    for category, rules in categories.items():
        for rule in rules:
            if rule['class'] != category:
                raise ValueError('category does not match with rule class: %s vs %s' % (category, rule['class']))
    return categories


def load_model(path):
    if os.path.isfile(path):
        return load_zipped_model(path)
    elif os.path.isdir(path):
        target_categories = load_categories(os.path.join(path, 'targets'))
        element_categories = load_categories(os.path.join(path, 'elements'))
        framenet = load_framenet(os.path.join(path, 'framenet.json'))
        return framenet, target_categories, element_categories
    raise Exception('invalid model path %s' % path)


def match_category(categories, features, selected=None):
    matched = []
    for category, rules in categories.items() if selected is None else ((k,categories[k]) for k in selected if k in categories):
        for rule in rules:
            match = True
            for k,v in rule['data'].items():
                if k not in features:
                    match = False
                    break
                if v != features[k]:
                    match = False
                    break
            if match:
                matched.append(category)
    return matched


def parse_frames(sentence, maxdist=3, selected_frames=None, verbose=False):

    targets = []
    elements = []

    targets = defaultdict(set)
    elements = defaultdict(set)
    element_rows = defaultdict(set)
    frame_candidates = []

    selected_elements = set()
    for row in sentence.rows:
        target_features = extract_features(row, sentence)
        target_candidates = match_category(target_categories, target_features, selected=selected_frames)
        # target_candidates = match_category(target_categories, target_features, selected=[sentence.frame.frame])     # selected attiecas tikai uz treniņdatiem
        # target_candidates = match_category(target_categories, target_features)
        if target_candidates:
            # targets.append([row, target_candidates])
            targets[row.n] = set(target_candidates)
            for target_candidate in target_candidates:
                selected_elements |= framenet[target_candidate]
                frame_candidates.append(Dict(n=row.n, fram=target_candidate))

    for row in sentence.rows:
        for frame in frame_candidates:
            element_features = extract_features(row, sentence, frame)
            element_candidates = match_category(element_categories, element_features, selected_elements)
            if element_candidates:
                # elements.append([row, element_candidates])
                if not row.paths:
                    elements[row.n] = set(element_candidates)
                    for e in element_candidates:
                        element_rows[e].add(row.n)
                else:
                    filtered_element_candidates = []
                    for tn, ts in targets.items():
                        for target in ts:
                            for element_candidate in element_candidates:
                                if element_candidate not in framenet[target]:
                                    continue
                                if row.n == tn or len(row.paths[tn]) <= maxdist:
                                    filtered_element_candidates.append(element_candidate)
                    if filtered_element_candidates:
                        elements[row.n] |= set(filtered_element_candidates)
                        for e in filtered_element_candidates:
                            element_rows[e].add(row.n)


    frames = []

    for tn, ts in targets.items():
        trow = sentence.rows[tn]
        for t in ts:
            frame = Dict(n=tn, target=t, elements={})
            for element in framenet[t]:
                if element not in element_rows:
                    continue
                dist = None
                nearest = None
                for en in element_rows[element]:
                    if not nearest or not trow.paths or (dist is not None and len(trow.paths[en]) < dist):
                    # if not nearest or len(trow.paths[en]) < dist:
                        if trow.paths:
                            dist = len(trow.paths[en])
                        nearest = en
                if nearest is not None:
                    element_rows[element].remove(nearest)    # remove from list, will not be reused for other frame
                    frame.elements[element] = nearest
            if frame.elements:
                frames.append(frame)


    if verbose:
        for row in sentence.rows:
            # print(row.n, row.form, 'TARGET', ','.join(target_candidates), 'ELEMENT', ','.join(element_candidates))
            # print(row.n, row.form, 'TARGET', ','.join(targets.get(row.n) or []), 'ELEMENT', ','.join(elements.get(row.n) or []))
            print('% 2s  %-20s TARGET %-40s ELEMENT %-40s' % (row.n, row.form, ','.join(targets.get(row.n) or []), ','.join(elements.get(row.n) or [])))

    return frames


def prepare_sentence(source_sentence):
    rows = tokens = [Dict(
        n = 0,
        form = '__ROOT__',
        lemma = '__ROOT__',
        pos = 'ROOT',
        tag = None,
        features = None,
        parent = None,
        deptype = None,
        children = [],
        paths = defaultdict(tuple),
    )]
    for token in source_sentence['tokens']:
        token = Dict(
            n = token.get('index'),
            form = token.get('text') or token.get('form', ''),
            lemma = token.get('lemma'),
            pos = token.get('upos'),
            tag = token.get('tag'),
            # features = convert_conll_features(token.get('features')),
            features = convert_conll_features(token.get('ufeats')),
            parent = token.get('parent'),
            deptype = token.get('deprel'),
            children = [],
            paths = defaultdict(tuple),
        )
        tokens.append(token)
    # gather children
    for row in rows:
        if row.parent is not None:
            rows[row.parent].children.append(row.n)
    sentence = Dict(source=source_sentence, tokens=tokens, rows=tokens) # rows is required
    compute_paths(sentence.rows) # compute_paths(sentence): row in sentence.rows: row.n, row.children, row.parent, row.paths
    return sentence


extract_features, framenet, target_categories, element_categories = None, None, None, None

model_path = 'model.zip'

def init(model=model_path):
    global extract_features, framenet, target_categories, element_categories, model_path

    if extract_features and framenet and target_categories and element_categories:
        return

    log.info('Loading model %s', model)

    if model.split(os.path.sep, 1)[0] in ('', '.', '..'):
        model = os.path.abspath(model)
    else:
        model = os.path.join(basedir, model)

    if model not in sys.path:
        sys.path.insert(0, model)
    from features import extract_features
    try:
        framenet, target_categories, element_categories = load_model(model)
        model_path = model
    except:
        log.critical("unable to load model: %s", model)


def parse(sentence, verbose=False):
    init()
    return parse_frames(prepare_sentence(sentence), verbose=verbose)



if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')

    log.setLevel(logging.DEBUG)

    import argparse

    parser = argparse.ArgumentParser(description='Frame Parser', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--model', '-m', dest='model', metavar='MODEL', type=str, default=os.environ.get('MODEL', 'model.zip'),
            help='path to model')
    parser.add_argument('--verbose', '-v', action='store_true', help='verbose mode')
    parser.add_argument('files', metavar='FILES', type=str, nargs='*', help='JSON file with data')

    args = parser.parse_args()

    if not args.model:
        log.critical("model not specified")
        quit(1)

    init(args.model)

    if not framenet:
        # load local framenet
        framenet = load_framenet('framenet.json')

    if not args.files:
        from conll import load_dir
        log.info('Loading CONLLs ...')
        all_conll_sentences = list(load_dir())
        log.info('%i CONLL sentences loaded', len(all_conll_sentences))
        for sentence in all_conll_sentences:
            frames = parse_frames(sentence, verbose=args.verbose)
            print('FRAMES:', frames)

    for fn in args.files:
        with open(fn, 'r') as f:
            data = json.load(f, object_hook=Dict)
        for sentence in data.sentences:
            # sentence = prepare_sentence(sentence)
            # frames = parse_frames(sentence, verbose=args.verbose)
            frames = parse(sentence, verbose=args.verbose)
            print('FRAMES:', frames)
