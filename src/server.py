import os
import logging
from flask import Flask, jsonify, request
from flask_cors import CORS
import frame_parser

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger('api')

PORT = os.getenv('PORT', '9501')
HOST = os.getenv('HOST', '0.0.0.0')

app = Flask(__name__)
CORS(app)


@app.route('/')
def index():
    return 'API is working'


@app.route('/api/process', methods=['GET', 'POST'])
def api_process():
    req = request.get_json(force=True, silent=True) or request.values
    doc = req['data']
    text = doc.get('text') or ''
    logger.info('process: {}'.format(repr(text[:20])))
    sentences = doc['sentences']
    try:
        for sentence in sentences:
            frames = frame_parser.parse(sentence)
            sentence['frames'] = frames
    except Exception as e:
        logger.exception('Exception on {}... {}'.format(repr(text), e))
        return 'Error processing data', 500

    return jsonify({'data': doc})


if __name__ == '__main__':
    print('RUN API %r %r' % (HOST, PORT))
    app.run(host=HOST, port=int(PORT), debug=False, threaded=False)
