FROM python:3.7.2-alpine3.9

WORKDIR /app/src

COPY requirements.txt /app/requirements.txt

RUN pip install --upgrade pip && pip install -r /app/requirements.txt

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV LANGUAGE=C.UTF-8
ENV PYTHONIOENCODING=utf-8
ENV PYTHONUNBUFFERED=1
ENV PORT=80

COPY src /app/src/

CMD gunicorn server:app -c gunicorn_config.py -w ${GUNICORN_WORKERS:=1} --timeout ${GUNICORN_TIMEOUT:=300} -b ${HOST:=0.0.0.0}:${PORT:=80} ${GUNICORN_RELOAD:=""}
